#!/usr/bin/bash

set -eu pipefail

build_tool="docker: $(docker system info --format 'v{{.ServerVersion}}@{{.Name}}/{{.ClientInfo.Context}}')"
ref="$(git rev-parse HEAD)"
branch="$(git branch --show-current)"

revision="${branch}-${ref}"

if [ "$(git status --porcelain | wc -l)" != "0" ]; then
	revision="${revision}+devel"
fi

if [ "$(git log --oneline  origin.. | wc -l)" != "0" ]; then
    revision="${revision}+needpush"
fi


docker build \
    --label org.opencontainers.image.revision="$revision" \
    --label org.opencontainers.image.created="$(date --rfc-3339='ns')" \
    --label org.opencontainers.image.build-tool="${build_tool}" \
	-t mygitserver:alma9 .

