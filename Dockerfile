FROM docker.io/library/almalinux:9

LABEL org.opencontainers.image.title="My (c)Git server"
LABEL org.opencontainers.image.authors="FC <me@fccagou.fr>"
LABEL org.opencontainers.image.source="https://gitlab.com/fccagou/mygitserver"
LABEL org.opencontainers.image.description="Local git server using cgit with apache"
LABEL org.opencontainers.image.documentation="https://gitlab.com/fccagou/mygitserver"
LABEL org.opencontainers.image.os="almalinux9"
LABEL org.opencontainers.image.dependencies="python:3, almalinux:9"


RUN dnf update -y \
    && dnf install -y epel-release \
	&& dnf install --enablerepo=crb -y \
	    findutils cgit git httpd \
		highlight python3-markdown python3-pygments \
	&& dnf clean all \
	&& rm -rf /var/cache/yum

COPY mygitserver /mygitserver
RUN chmod 755 /mygitserver

RUN useradd -m -d /srv/cgit cgit

USER cgit
RUN /mygitserver --update /srv/cgit
EXPOSE 8080
ENTRYPOINT ["/mygitserver", "--listen", "0.0.0.0" ]
CMD ["--update", "--run", "--no-chown", "/srv/cgit"]
